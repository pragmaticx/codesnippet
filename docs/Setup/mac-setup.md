# Mac Setup

1. Install one Note and get the instruction  
    a. Change it to a website 
2. Change terminal settings (minimum)  
    a. Default to HomeBrew  
    b. Text tab -> Change font to Melano 18  
    c. Shell Tab -> change to close if exited cleanly  
3. Install 1Password /BitWarden
4. Install oh-my-zsh 
    a. [Install Ohmyz](https://ohmyz.sh/#install)
```
sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh"
```
```
import tensorflow as tf
```        

    
5. Install VS code 
    a. Setup to launch from terminal - https://code.visualstudio.com/docs/setup/mac
    b. Install extensions for code 
6. https://gitlab.com/asrafuli/scripts/-/blob/main/setup_mac.sh
7. Install magnet, change trackpad setting, change name 
8. Install onedrive and one note -- need to write instruction to do JDK and run it properly 
9. Install something to check HDD size and track as I install new s/w
10. Install Java (JdiskReport) 
11. https://sourabhbajaj.com/mac-setup/
    a. Install commandline tool for xcode 
    b. Install brew 
12. brew install zsh
13. brew install git
14. brew install git-gui
15. brew install bash-completion
16. brew cask install visual-studio-code
17. brew install tree
18. brew install speedtest-cli
19. Install python (pyshim)
20. Start here - Python
21. Install office 
22. brew install --cask finicky

Using Ansible: 
https://www.talkingquickly.co.uk/2021/01/macos-setup-with-ansible/
	
	
#!/bin/sh
apt-get update
if ! type "docker" > /dev/null; then
  echo "Installing Docker"
  sudo apt-get -y install docker.io
  sudo apt-get -y install docker-ce
fi
sudo apt install net-tools
# get ips | search inet addr: | split on : get 2nd field | print out
ADDRESS=$(ifconfig wlp1s0 | grep 'inet' | cut -d: -f2 | awk '{ print $2}')
# if we have an emtpy address
if [[ -z "${ADDRESS}"]]; then
  # some prints won't have the addr portion, do a last ditch effort to get the ip
  # get ips | search inet | get first line | trim string | split on ' ' get the 2nd field | print out
  ADDRESS=$(ifconfig eth0 | grep 'inet' | awk 'NR == 1' | awk '{$1=$1};1' | cut -d ' ' -f 2 | awk '{ print $1}')
fi
docker swarm init --advertise-addr="$ADDRESS"
if ! type "git" > /dev/null; then
  echo "Installing Git"
  sudo apt-get -y install git
fi
DIRECTORY="prometheus-grafana-alertmanager-example"
if [ -d "$DIRECTORY" ]; then
  rm -rf "$DIRECTORY"
fi
echo "Cloning Project"
git clone https://github.com/PagerTree/prometheus-grafana-alertmanager-example.git
cd "$DIRECTORY"
echo "Making Utility scripts executable"
sudo chmod +x ./util/*.sh
echo "Starting Application"
docker stack deploy -c docker-compose.yml prom
echo "Waiting 5 seconds for services to come up"
sleep 5
while docker service ls | grep "0/1";  do sleep 3; echo "Waiting..."; done;
echo "You can now access your Grafana dashboard at http://$ADDRESS:3000"
