## Some MKDocs helpful links 

* [mkdocs](https://www.mkdocs.org)
* [mkdocs material](https://squidfunk.github.io/mkdocs-material/)
* [markdown guide](https://www.markdownguide.org)

